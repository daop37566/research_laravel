<?php

namespace App\Events;

use App\Models\Films;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CustomerOrder
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $bill;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($userBillInfor)
    {
        $this->bill = $userBillInfor;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
