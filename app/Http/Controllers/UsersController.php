<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Notifications\UserFollowed;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    /**
     * GET page Home :
     * @param null
     * @return Page , $user
     * **/
    public function index()
    {
        $users = User::where('id', '!=', auth()->user()->id)->get();
        return view('enduser.pages.index', compact('users'));
    }


    /**
     * Create Follow :
     * @param $user
     * @return Page
     * **/
    public function follow(User $user)
    {
        $follower = auth()->user();
        if ($follower->id == $user->id) {
            return back()->withError("You can't follow yourself");
        }
        if(!$follower->isFollowing($user)) {
            $follower->following()->attach($user->id);
            // sending a notification
            $user->notify(new UserFollowed($follower));

            return back()->withSuccess("You are now friends with {$user->name}");
        }
        return back()->withError("You are already following {$user->name}");
    }


    /**
     * Create UnFollow :
     * @param $user
     * @return Page
     * **/
    public function unfollow(User $user)
    {
        $follower = auth()->user();
        if($follower->isFollowing($user)) {
            $follower->following()->detach($user->id);
            return back()->withSuccess("You are no longer friends with {$user->name}");
        }
        return back()->withError("You are not following {$user->name}");
    }

    /**
     * Get Notification unreadNotifications:
     * @param $user
     * @return $date
     * **/
    public function notifications()
    {
        return auth()->user()->unreadNotifications()->limit(5)->get()->toArray();
    }

}
