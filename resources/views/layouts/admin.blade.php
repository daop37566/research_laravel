<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DEMO</title>
    @if (Route::current()->getName() == 'login')
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    @else
    <link type="text/css" rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<link type="text/css" rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    @endif
    <link type="text/css" rel="stylesheet" href="{{ asset('/css/admin/metisMenu.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('/css/admin/timeline.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('/css/admin/dataTables.bootstrap.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('/css/admin/dataTables.responsive.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('/css/admin/global-admin.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('/css/admin/morris.css') }}">
    <link type="text/css" rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css">
    <link type="image/x-icon" rel="icon" href="{{ asset('/images/favicon.png') }}">
    @yield('link')
</head>
<body>
    @yield('content')
    <script type="text/javascript" src='https://code.jquery.com/jquery-3.6.0.min.js'></script>
    <script type="text/javascript" src="{{ asset('/js/admin/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/admin/metisMenu.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/admin/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/admin/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/admin/startmin.js') }}"></script>
    @yield('scripts')
</body>

</html>