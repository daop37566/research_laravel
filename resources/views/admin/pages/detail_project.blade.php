@extends('layouts.admin')
@section('title')
Stdio Huế
@endsection
@section('link')
@endsection
@section('content')
@include('common.admin.header')
<div id="page-wrapper">
    <div class="container-fluid" style="min-height: 1000px;">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-header">
                    <a href="{{ url('/dashboard') }}"><i class="fa fa-home fa-fw"></i></a>/ 
                    <a href="{{ url('/banners') }}"><strong>List Banner</strong></a> / 
                    <strong>Detail</strong>
                </h4>
            </div>
        </div>
        <div class="row">
            @if ($errors->any())
                <p class="text-center alert alert-danger">{{ $errors->first() }}</p>
            @endif
            <form class="col-lg-12" action="/banners/{{ $project->id }}" method="POST" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <img class="col-lg-6 col-md-6 col-sm-6 col-xs-12" src="{{ getImage('banner', $project -> picture) }}" alt="Project Image">
                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label>Name:</label>
                        <input type="text" class="form-control" name="name" value="{{ $project->name }}" required>
                    </div>
                    <div class="form-group">
                        <label>Image:</label>
                        <input type="file" class="form-control" name="image">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary center-block">
                            <strong>Edit</strong>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /.container-fluid -->
    @include('common.admin.footer')
</div>
@endsection
@section('scripts')
@endsection