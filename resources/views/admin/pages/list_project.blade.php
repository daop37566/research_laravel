@extends('layouts.admin')
@section('title')
Stdio Huế
@endsection
@section('link')
@endsection
@section('content')
@include('common.admin.header')
<!-- Page Content -->
@if ($errors->any())
<script type="text/javascript">
    alert('{{ $errors->first() }}');
</script>
@endif
<div id="page-wrapper">
    <div class="container-fluid" style="min-height: 1000px">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-header">
                    <a href="{{ url('/admin/dashboard') }}"><i class="fa fa-home fa-fw"></i></a>/ 
                    <strong>List Banner</strong>
                </h4>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                            <strong>Add Banner</strong>
                        </button>
                        <!-- Modal -->
                        <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel"><strong>Add new project</strong></h4>
                                    </div>
                                    <form action="{{ url('/banners') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Name *</label>
                                                <input type="text" class="form-control" name="name" required>
                                                <p class="help-block">Example: Bidu</p>
                                            </div>
                                            <div class="form-group">
                                                <label>Image *</label>
                                                <input type="file" class="form-control" name="image" required>
                                                <p class="help-block">Example: </p>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                            <button type="submit" class="btn btn-primary">Add</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Image</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($banners as $banner)
                                        @if ($loop->index % 2 == 0)
                                        <tr class="even">
                                        @else
                                        <tr class="odd">
                                        @endif
                                            <td>{{ $banner->id }}</td>
                                            <td>{{ $banner->name }}</td>
                                            <td>{{ $banner->type }}</td>
                                            <td class="cell-hide">{{ $banner->overview }}</td>
                                            <td><img class="center-block" style="width: 200px; height: 200px;" src="{{ getImage('banner', $banner -> picture) }}" alt="banner Image"></td>
                                            <td>
                                                <a href="/banners/{{ $banner->id }}">
                                                    <button type="submit">
                                                        <i class="fa fa-info-circle fa-fw"></i> Detail
                                                    </button>
                                                </a> 
                                                <form action="/banners/{{ $banner->id }}" method="POST" onsubmit="return confirm('Delete this banner, are you sure ?')">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button type="submit">
                                                        <i class="fa fa-trash-alt fa-fw"></i> 
                                                        Delete
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    @include('common.admin.footer')
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
</script>
@endsection