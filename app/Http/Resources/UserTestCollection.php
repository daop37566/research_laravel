<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserTestCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray1($request)
    {
        return [
            'data' => $this->collection,
            'response' => [
                'status' => 'success',
                'code' => 200,
                'P' => 1
            ],
        ];
    }
}
