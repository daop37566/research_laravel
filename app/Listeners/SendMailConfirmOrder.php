<?php

namespace App\Listeners;

use App\Events\CustomerOrder;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;

class SendMailConfirmOrder implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\CustomerOrder  $event
     * @return void
     */
    public function handle(CustomerOrder $event)
    {
        Mail::send('mails/notify_couple_done_card', ['đâjk'], function($msg) {
            $msg->to('daop37566@gmail.com')->subject("WEB招待状確認依頼");
        });
    }
}
