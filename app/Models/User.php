<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function routeNotificationForMail($notification)
    {
        // Return email address only...
        return $this->email;

        // Return email address and name...
        return [$this->email => $this->name];
    }



    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        // 'name' => 'integer'
    ];

    // public function getFullNameAttribute()
    // {
    //     return "{$this->name}";
    // }

    // public function getFirstNameAttribute()
    // {
    //     return  "{$this->name} . =========== . {$this->email}";
    // }

    // public function setFirstNameAttribute($value)
    // {
    //     $this->attributes['name'] = strtolower($value);
    // }

    // public function setCongTyAttribute($value)
    // {
    //     $this->attributes['congty'] = strtolower($value);
    // }

    // public function setPasswordAttribute($value)
    // {
    //     $this->attributes['password'] = $value;
    // }

    // public function messages()
    // {
    //     return $this->hasMany(Message::class);
    // }


    // public function followers()
    // {
    //     return $this->belongsToMany(self::class, 'followers', 'follows_id', 'user_id')
    //                 ->withTimestamps();
    // }

    // public function follows()
    // {
    //     return $this->belongsToMany(self::class, 'followers', 'user_id', 'follows_id')
    //                 ->withTimestamps();
    // }

    // public function follow($userId)
    // {
    //     $this->follows()->attach($userId);
    //     return $this;
    // }

    // public function unfollow($userId)
    // {
    //     $this->follows()->detach($userId);
    //     return $this;
    // }

    // public function isFollowing($userId)
    // {
    //     return (boolean) $this->follows()->where('follows_id', $userId)->first(['id']);
    // }

    // public function receivesBroadcastNotificationsOn()
    // {
    //     return 'App.Models.User.'.$this->id;
    // }

    public function following()
    {
        return $this->belongsToMany(User::class, 'followers', 'user_id', 'follows_id')->withTimestamps();
    }

    public function followers()
    {
        return $this->belongsToMany(User::class, 'followers', 'follows_id', 'user_id')->withTimestamps();
    }

    public function isFollowing(User $user)
    {
        return !! $this->following()->where('follows_id', $user->id)->count();
    }

    public function isFollowedBy(User $user)
    {
        return !! $this->followers()->where('user_id', $user->id)->count();
    }
}
