<?php
namespace App\Helper;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
class CustomPagination 
{
    protected $total;
    protected $lastPage;
    protected $path = '/';
    protected $currentPage;


    public function __construct($items, $total, $perPage, $currentPage = null)
    {
    
        $this->total = $total;
        $this->perPage = $perPage;
        $this->lastPage = max((int) ceil($total / $perPage), 1);
        $this->path = $this->path !== '/' ? rtrim($this->path, '/') : $this->path;
        $this->currentPage = $this->setCurrentPage($currentPage);
        $this->items = $items instanceof Collection ? $items : Collection::make($items);

    }

    public function lastPage()
    {
        return $this->lastPage;
    }

    public function currentPage()
    {
        return $this->currentPage;
    }

    protected function setCurrentPage($currentPage)
    {
        
        $currentPage = $currentPage ?: 1;

        return $this->isValidPageNumber($currentPage) ? (int) $currentPage : 1;
    }

    protected function isValidPageNumber($page)
    {
        return $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false;
    }
    

    // protected function buildFragment()
    // {
    //     return $this->fragment ? '#'.$this->fragment : '';
    // }

    public function path()
    {
        return $this->path;
    }


    // public function url($page)
    // {
    //     if ($page <= 0) {
    //         $page = 1;
    //     }

    //     // If we have any extra query string key / value pairs that need to be added
    //     // onto the URL, we will put them in query string form and then attach it
    //     // to the URL. This allows for extra information like sortings storage.
    //     $parameters = [$this->pageName => $page];

    //     if (count($this->query) > 0) {
    //         $parameters = array_merge($this->query, $parameters);
    //     }
    //     return $this->path()
    //                     .(Str::contains($this->path(), '?') ? '&' : '?')
    //                     .Arr::query($parameters)
    //                     .$this->buildFragment();
    // }
}