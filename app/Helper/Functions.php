<?php

use App\Helper\CustomPagination;
use Illuminate\Support\Collection;

if (!function_exists('getImage')) {
    function getImage($folderUpload, $fileImage, $type=""){
        if(isset($type)){
            $path = public_path() . "/picture/" . $folderUpload . "/" . $type . "/" . $fileImage;
        }
        else{
            $path = public_path() . "/picture/" . $folderUpload . "/" . $fileImage;
        }
        if(isset($fileImage) && file_exists($path)){
            if(isset($type)){
                return asset("picture/" . $folderUpload . "/" . $type . "/" . $fileImage);
            }
            else{
                return asset("picture/" . $folderUpload . "/" . $fileImage);
            }
        }else{
            return asset("picture/default-image.jpg");
        }
    }
}

if(!function_exists('paginateCustom')){
    function paginateCustom($items, $perPage = 3, $page = null)
    {

        if(isset($_GET['page'])){
            $currentPage = $_GET['page']; 
        }else{
            $currentPage = NULL;
        }
        $page = $page ?: ((int) $currentPage ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        
        return new CustomPagination($items->forPage($page, $perPage), $items->count(), $perPage, $page);
    }
}


    
    

