<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function editComment()
    {
        $comment = Comment::find(1); // tìm comment có id = 1
        /// kiểm tra user đang login có phải đã đăng comment không thì mới cho edit
        if (Gate::allows('edit-comment', $comment)) {
            //// bạn có quyền edit trong này
        } else {
            /// Ban khong co quyen chinh sua comment
        }

        // Xem 1 người dùng bất kì
        // if (Gate::forUser($user)->allows('edit-comment', $comment)) {
        //     echo "Người dùng này được phép chỉnh sửa comment";
        // }

        // if (Gate::forUser($user)->denies('edit-comment', $comment)) {
        //     echo "Người dùng này không được phép chỉnh sửa comment";
        // }
    }

    // public function update(Request $request, Post $post)
    // {
    //     $this->authorize('update', $post);

    //     // The current user can update the blog post...
    // }
}
