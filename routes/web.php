<?php

use App\Models\User;
use App\Notifications\InvoicePaid;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('category', [CategoryController::class, 'index'])->middleware('checkage');

// Route::group(['prefix' => 'laravel-filemanager', 'middleware' => 'checkage'], function () {
//     Route::get('category/them', [CategoryController::class, 'add']);
//     Route::get('category/update/{id}/{parent?}', [CategoryController::class, 'update']);
//     Route::get('get-data', [CategoryController::class, 'GetData']);
// });





Auth::routes();

Route::get('send-sms-notification', [App\Http\Controllers\HomeController::class, 'sendSmsNotificaition']);

Route::get('message', [App\Http\Controllers\HomeController::class, 'index'])->name('page.index');
Route::post('message', [App\Http\Controllers\HomeController::class, 'store'])->name('message.store');

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



Route::get('/notification', function () {
    $invoice = User::find(2);

    return (new InvoicePaid($invoice))
                ->toMail($invoice->email);
});

Route::post('/broadcasting/auth', function () {
    return Auth::user();
 });

// policy middleware
// Route::put('/post/{post}', function (Post $post) {
//     // The current user may update the post...
//  })->middleware('can:update,post');

Route::get('/has-many', [App\Http\Controllers\HomeController::class, 'hasMany'])->name('page.hasMany');

Route::get('/morpho-to', [App\Http\Controllers\HomeController::class, 'morphoTo'])->name('page.morphoTo');

Route::get('/get-name', [App\Http\Controllers\UserController::class, 'getName'])->name('getName');
Route::get('/set-name', [App\Http\Controllers\UserController::class, 'setName'])->name('setName');


Route::group(['prefix' => 'banners'], function() {
    Route::get('', 'BannerController@listBanner')->name('list_Banner');
    Route::post('', 'BannerController@createBanner')->name('create_Banner');
    Route::get('{id}', 'BannerController@readBanner')->name('read_Banner');
    Route::put('{id}', 'BannerController@updateBanner')->name('update_Banner');
    Route::delete('{id}', 'BannerController@deleteBanner')->name('delete_Banner');
});

//Login google
Route::get('/login-google', 'UserController@login_google')->name('login_google');
Route::get('/google/callback','UserController@callback_google');

//Login FACEBOOK
Route::get('/login-facebook', 'UserController@login_facebook')->name('login_facebook');
Route::get('/facebook/callback','UserController@callback_facebook');


Route::get('/orders', function (Request $request) {
    echo "Loại 4";
})->middleware('client:check-status,your-scope');

Route::get('/demos', function (Request $request) {
    $response = Http::timeout(1000)->get('https://api.themoviedb.org/3/movie/256/lists',[
        'api_key' => '68a4b1cf8c692e9576256823e6106586',
        'page' => 1,
        'language' => 'en-US'
    ]);
    return json_decode((string) $response->successful(), true);
 });


Route::get('/callback', function (Request $request) {
    $http = new GuzzleHttp\Client;

    $response = $http->get('https://api.themoviedb.org/3/movie/256/lists?api_key=68a4b1cf8c692e9576256823e6106586&language=en-US&page=1', [
    ]);

    return json_decode((string) $response->getBody(), true);
 });


 Route::get('/callback1', function (Request $request) {
    $response = Http::get('https://api.themoviedb.org/3/movie/256/lists?api_key=68a4b1cf8c692e9576256823e6106586&language=en-US&page=1', [
    ]);
    return json_decode((string) $response->successful(), true);
 });


 Route::get('/demo1', function (Request $request) {
     $photo = 'ahdkahdkahjk';
    $response = Http::withBody(
        base64_encode($photo), 'image/jpeg'
    )->post('http://example.com/photo');
    return base64_encode($photo);
 });




 // DEMO HTTP CLIENT

Route::get('/httpGET', function (Request $request) {
    $http = new GuzzleHttp\Client;

    $response = $http->get('https://621206a001ccdac0743035e0.mockapi.io/api/v1/user', [
    ]);

    return json_decode((string) $response->getBody(), true);
 });

 Route::get('/httpPOST', function (Request $request) {
    $http = new GuzzleHttp\Client;

    $response = $http->post('https://621206a001ccdac0743035e0.mockapi.io/api/v1/user', [
        'name' => 'Dao Duy Phương',
        'avatar' => 'https://cdn.fakercloud.com/avatars/kolage_128.jpg'
    ]);

    return json_decode((string) $response->getStatusCode(), true);
 });

 Route::get('/httpPUT', function (Request $request) {
    $http = new GuzzleHttp\Client;

    $response = $http->put('https://621206a001ccdac0743035e0.mockapi.io/api/v1/user/1', [
        'name' => 'Dao Duy Phương',
    ]);

    return json_decode((string) $response->getStatusCode(), true);
 });

 Route::get('/httpDELETE', function (Request $request) {
    $http = new GuzzleHttp\Client;

    $response = $http->delete('https://621206a001ccdac0743035e0.mockapi.io/api/v1/user/1', [
    ]);

    return json_decode((string) $response->getStatusCode(), true);
 });

//  $response = Http::withHeaders([
//     'X-First' => 'foo',
//     'X-Second' => 'bar'
// ])->post('http://example.com/users', [
//     'name' => 'Taylor',
// ]);

// $response = Http::acceptJson()->get('http://example.com/users');

// // $response = Http::withToken('token')->post(...);

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');




// DEMO NOTIFICATION

//...
Route::group(['middleware' => 'auth'], function () {
    Route::get('users', 'UsersController@index')->name('users');
    Route::post('users/{user}/follow', 'UsersController@follow')->name('follow');
    Route::delete('users/{user}/unfollow', 'UsersController@unfollow')->name('unfollow');
    Route::get('/notifications', 'UsersController@notifications');
});
