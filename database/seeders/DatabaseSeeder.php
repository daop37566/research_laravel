<?php

namespace Database\Seeders;

use App\Models\Films;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\Banner::factory(2)->create();
        // $this->call([
        //     UserSeeder::class
        // ]);

        for($i = 0 ; $i < 1000 ; $i++){
            Films::create([
                'name' => 'A' .$i,
                'director' => 'B' .$i,
                'actor' => 'C' .$i,
                'duration' => 100,
                'filmview' => 0,
                'year' => 2020,
                'description' => 'âdahda',
                'trailer' => 'https://adadahda',
                'image' => '4BaHioFiILVh.jpg',
                'imageBanner' => 'r70WpwmVCySx.jpg',
                'ttstatus' => 'HD VietSub',
                'session' => 1,
                'slug' => 'dau-la-dai-luc-3d',
                'kindoffilm_id' => 1
            ]);
        }

        // $this->call(UserSeeder::class);
    }
}
