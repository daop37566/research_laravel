<?php

namespace App\Http\Requests;

use App\Http\Requests\ApiRequest;

class LoginUserApiRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required|min:6|max:20'
        ];
    }
    public function messages()
    {
        return [
            'email.required' => 'A title is required',
            'password.required' => 'A message is required',
        ];
    }
    public function attributes()
    {
        return [
            'email' => 'email address',
        ];
    }
}
