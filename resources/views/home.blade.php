@extends('layouts.app')

@section('content')

@php 
use Illuminate\Support\Facades\Cache;
Cache::put('domain', 'toidicode.com', -600);

$domain = Cache::get('domain','dam');
dd($domain);
@endphp
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>

</div>
@endsection


