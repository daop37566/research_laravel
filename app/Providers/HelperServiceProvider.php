<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        // 1 file : 
        $file = app_path('Helper/functions.php');
        if (file_exists($file)) {
            require_once($file);
        }
        // Nhiu file 
        foreach (glob(app_path() . '/Helper/*.php') as $file) {
            require_once($file);
        }

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
