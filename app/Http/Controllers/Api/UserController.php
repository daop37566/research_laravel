<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helper\Functions;
use App\Http\Resources\UserResource as ResourcesUserResource;
use App\Http\Resources\UserTestCollection;
use App\Models\Films;
use App\Models\User as ModelsUser;

class UserController extends Controller
{
    /**
     * Display the specified resource.
     * Not display password and remember_token because in User model was hidden these
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $users =  ModelsUser::all();

        foreach ($users as $key => $user){
            if($user -> picture){
                $user -> picture = getImage("user", $user -> picture);
            }
        }

        return new UserTestCollection($users);
    }

    // public function store(Request $request)
    // {
    //     if(DB::table('user')->insert($request->all())){
    //         return response()->json("User has been created",201);
    //     }
    //     else{
    //         return response()->json("Can not create");
    //     }
    // }

    // public function show($id)
    // {
    //     //
    // }

    // public function update(Request $request, $id)
    // {
    //     //
    // }

    // public function destroy($id)
    // {
    //     //
    // }
}
