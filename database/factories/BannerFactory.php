<?php
namespace Database\Factories;

use App\Models\Banner;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class BannerFactory extends Factory
{

    protected $model = Banner::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */

    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'slug' => Str::slug($this->faker->name),
            'picture' => $this->faker->image(),
            'created_at' => $this->faker->dateTimeThisMonth()->format('Y-m-d H:i:s'),
            'updated_at' => $this->faker->dateTimeThisMonth()->format('Y-m-d H:i:s'),
        ];
    }
}