<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\Http\Requests\LoginUserApiRequest;
use App\Models\User as ModelsUser;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Str;
use GuzzleHttp\Client;
use Socialite;


class UserApiController extends BaseController
{
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Register User :
     * @param $request
     * @return JSON
     * **/
    public function postRegister(Request $request)
    {

        try {

            $validator = Validator::make($request->all(), [
                'username' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required',
                // 'confirm_password' => 'required|same:password',
            ]);

            if ($validator->fails()) {
                return $this->sendError(self::VALIDATION_ERROR, $validator->errors());
            }

            if (!in_array($request->age, $this->age)) {
                return $this->sendError(self::ERROR, 'Sai định dạng tuổi');
            }

            $user = new ModelsUser();
            $user->username = $request->username;
            $user->email = $request->email;
            $user->password = Hash::make(123);
            $user->save();
            return $this->sendResponse([], 'Tạo tài khoản thành công');
        } catch (\Exception $e) {

            return $this->sendError(self::ERROR, $e->getMessage());
        }
    }

    /**
     * Login :
     * @param $request
     * @return JSON
     * **/
    public function login(LoginUserApiRequest $request)
    {

        $credentials = request(['email', 'password']);

        if (!Auth::attempt($credentials)) {
            return $this->sendError('Unauthorized', [], 401);
        }

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }

        $token->save();

        return $this->sendResponse([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ], 'Đăng nhập thành công');
    }

    /**
     * Logout :
     * @param $request
     * @return JSON
     * **/
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return $this->sendResponse([], 'Đăng xuất thành công');
    }


    // public function redirectToProvider()
    // {
    //     return Socialite::with('google')->redirect();
    // }


        /**
     * @param String $social_token
     * @return void
     */
    public function checkGoogle($social_token)
    {
        try {
            $checkToken = $this->client->get("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=107953360879698464376");
            $responseGoogle = json_decode($checkToken->getBody()->getContents(), true);

            return $this->checkUserByEmail($responseGoogle);
        } catch (\Exception $e) {
            return $this->sendError(self::ERROR, $e->getMessage());
        }
    }

    /**
     * Check Login by Email :
     * @param $profile
     * @return Json
     * **/
    public function checkUserByEmail($profile)
    {
        $user = ModelsUser::where('email', $profile['email'])->first();
        if (!$user) {
            $user = ModelsUser::create([
                'name' => $profile['name'],
                'email' => $profile['email'],
                'password' => bcrypt(Str::random(8)),
            ]);
        }

        $user->forceFill([
            'verified' => true,
            'email' => $user['email'],
            'activated_at' => Carbon::now(),
        ])->save();
        $tokenResult = $user->createToken('Personal Access Client');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addMonth();
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

}
