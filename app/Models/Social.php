<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    public $timestamps = false;
    protected $fillable =
        ['provider_user_id', 'provider', 'user_id'];

    protected $table = "social";

    public function login(){
        return $this->belongsTo('App\Models\User', "user_id");
    }
}
