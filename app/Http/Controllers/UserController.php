<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\LoginUserRequest;
use App\Http\Requests\ValidateCreateUser;
use App\Models\Social;
use App\Models\User;
use Socialite;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{

    /**
     * Check Login :
     * @param null
     * @return Page
     * **/
    function getLogin() {
        if (Auth::check()) {
            return redirect()->route('dashboard');
        } else {
            return view('pages.admin.login');
        }
    }

    /**
     * Login :
     * @param $request
     * @return Page
     * **/
    function postLogin(LoginUserRequest $request) {
        $credentials = array('email' => $request->email, 'password' => $request->password);
        $remember = isset($request->remember);
        if (Auth::attempt($credentials, $remember)) {
            // $user = Auth::user();
            // // $user->last_login = now();
            // $user->save();
            return redirect()->route('dashboard');
        } else {
            return redirect()->back()->withErrors('Invalid email or password');
        }
    }

    /**
     * GET Name : Demo mutator  :
     * @param null
     * @return dd() ;
     * **/
    function getName()
    {
        $user = User::find(1);


        $user->cong_ty = "adgda";
        // $name =  $user->first_name;
        dd($user);
        // dd(gettype($user->name));
        // $firstName = $user->getFirstNameAttribute();
        // echo($firstName);
    }


    /**
     * create Logout :
     * @param null
     * @return Page
     * **/
    function logout() {
        Auth::logout();
        return redirect()->route('login');
    }

    /**
     * create User Login  :
     * @param $request
     * @return Page
     * **/
    function createUser(ValidateCreateUser $request) {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Crypt::encrypt($request->password);
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $file_name = '/images/' . strval(strtotime(now())) . '.' . $file->getClientOriginalExtension();
            Storage::disk('
            ')->put($file_name, file_get_contents($file));
            $user->avatar = $file_name;
        }
        $user->save();
        return redirect()->back();
    }

    // function readUser($id) {
    //     $user = User::findOrFail($id);
    //     return view('pages.admin.detail_user', compact('user'));
    // }

    // function updateUser(ValidateUpdateUser $request, $id) {
    //     $user = User::findOrFail($id);
    //     $user->name = $request->name;
    //     $user->email = $request->email;
    //     if (isset($request->password)) {
    //         $user->password = Hash::make($request->password);
    //     }
    //     if ($request->hasFile('avatar')) {
    //         if (Storage::disk('public')->exists($user->avatar)) {
    //             Storage::disk('public')->delete($user->avatar);
    //         }
    //         $file = $request->file('avatar');
    //         $file_name = '/images/' . strval(strtotime(now())) . '.' . $file->getClientOriginalExtension();
    //         Storage::disk('public')->put($file_name, file_get_contents($file));
    //         $user->avatar = $file_name;
    //     }
    //     $user->save();
    //     return redirect()->back();
    // }

    // function deleteUser($id) {
    //     $user = User::findOrFail($id);
    //     if (Storage::disk('public')->exists($user->avatar)) {
    //         Storage::disk('public')->delete($user->avatar);
    //     }
    //     $user->delete();
    //     return redirect()->back();
    // }


    /**
     * create redirect Login Google :
     * @param null
     * @return Form Login
     * **/
    public function login_google(){
        return Socialite::driver('google')->redirect();
    }

     /**
     * create redirect Login Google :
     * @param null
     * @return Form Login
     * **/
    public function callback_google(){
        $users = Socialite::driver('google')->stateless()->user();
        // return $users->id;

        $authuser = $this->findorCreateUser($users, 'google');
        $accountName =  User::where('email',$users->email)->first();

        Session::put ('google_login',$accountName->name);
        Session::put('google_id',$accountName->id);
        return redirect('/')->with('message', 'Đăng nhập Admin thành công');

    }

    /**
     * create User Login Facebook :
     * @param $users , $provider
     * @return Page
     * **/
    public function findorCreateUser($users, $provider){
        $authUser =  Social::where('provider_user_id', $users->id)->first();
        if ($authUser){
           return $authUser;
        }
        $hieu = new Social([
            "provider_user_id" => $users->id,
            "provider" => strtoupper($provider),
        ]);

        $orang = User::where('email',$users->email)->first();

        if(!$orang){
            $orang = User::create([
                'name' => $users->name,
                'email'=> $users->email,
                'password'=> '',
                // 'status'  => 'active'
            ]);
        }
        $hieu->login()->associate($orang);
        $hieu->save();

        $accountName = User::where('email',$users->email)->first();
        Session::put ('google_login',$accountName->name);
        Session::put('google_id',$accountName->id);

        return redirect('/')->with('message', "Đăng nhập Admin thành công");

    }


    /**
     * create redirect Login Facebook :
     * @param null
     * @return Form Login
     * **/
    public function login_facebook(){
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * create redirect Callback Facebook :
     * @param null
     * @return Page
     * **/
    public function callback_facebook(){
        $users = Socialite::driver('facebook')->user();
        // return $users->id;

        $authuser = $this->findorCreateUserFB($users, 'facebook');
        $accountName =  User::where('email',$users->email)->first();

        Session::put ('facebook_login',$accountName->name);
        Session::put('facebook_id',$accountName->id);
        return redirect('/')->with('message', 'Đăng nhập Admin thành công');

    }

    /**
     * create User Login Facebook :
     * @param $users , $provider
     * @return Page
     * **/
    public function findorCreateUserFB($users, $provider){
        $authUser =  Social::where('provider_user_id', $users->id)->first();
        if ($authUser){
           return $authUser;
        }
        $hieu = new Social([
            "provider_user_id" => $users->id,
            "provider" => strtoupper($provider),
        ]);

        $orang = User::where('email',$users->email)->first();

        if(!$orang){
            $orang = User::create([
                'name' => $users->name,
                'email'=> $users->email,
                'password'=> '',
                // 'status'  => 'active'
            ]);
        }
        $hieu->login()->associate($orang);
        $hieu->save();

        $accountName = User::where('email',$users->email)->first();
        Session::put ('facebook_login',$accountName->name);
        Session::put('facebook_id',$accountName->id);

        return redirect('/')->with('message', "Đăng nhập Admin thành công");

    }

}
