<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="navbar-header">
        <div class="navbar-brand">DEMO LARAVEL</div>
    </div>

    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>

    <!-- Top Navigation: Right Menu -->
    <ul class="nav navbar-right navbar-top-links">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> Hello, PHUONG <b class="caret"></b>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li>
                    <a href="/admin/user/"><i class="fa fa-user fa-fw"></i> User Profile</a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href=""><i class="fa fa-sign-out-alt fa-fw"></i> Logout</a>
                </li>
            </ul>
        </li>
    </ul>

    <!-- Sidebar -->
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse collapse">

            <ul class="nav" id="side-menu">
                
                <li>
                    <a href="{{ url('/admin/dashboard') }}"><i class="fa fa-tachometer-alt fa-fw"></i> Dashboard</a>
                </li>
                <li>
                    <a href="{{ url('/banners') }}"><i class="fa fa-user fa-fw"></i> Banners </a>
                </li>
            </ul>
            
        </div>
    </div>
</nav>