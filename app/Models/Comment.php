<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "comments";
    protected $fillable = ['name','like'];

    public function commentable() {
        return $this->morphTo();
    }

    public function comments() {
        return $this->morphMany(Comment::class, 'commentable');
    }
}