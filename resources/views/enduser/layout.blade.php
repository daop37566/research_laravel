<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="vi" lang="vi">
<!-- Mirrored from animefull.tv/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 11 Sep 2021 06:05:10 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    <meta charset="utf-8">
    <title>AnimeFull - Xem anime mới vietsub nhanh nhất</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
{{--
    <meta property="og:title" content="DuyPhuong Full - Xem anime mới vietsub nhanh nhất" />
    <meta property="og:image" content="Theme/images/icon-animefull-fb-2565e1f.png?v=2" /> --}}
    {{-- <meta name="csrf-token" content="{{ csrf_token() }}"> --}}

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.6.0/css/all.min.css" integrity="sha512-ykRBEJhyZ+B/BIJcBuOyUoIxh0OfdICfHPnPfBy7eIiyJv536ojTCsgX8aqrLQ9VJZHGz4tvYyzOM0lkgmQZGw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" type="text/css" href="{{ asset("enduser/Theme/css/mainfdc4.css?v=0.78w62237222222222224232285632843422") }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset("enduser/Theme/css/user3aa4.css") }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset("enduser/Theme/css/style.css") }}" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Scripts -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script>
        window.Laravel = <?= json_encode(['csrfToken' => csrf_token() ]); ?>
    </script>

    <script>
        window.Laravel.id = <?= auth()->user()->id; ?>
    </script>

    // <script src="{{ asset('js/app.js') }}" defer></script>



</head>

<body>


    <div id="app">
        <div id="main-content">
        <div id="content">
            <div class="container">
                @yield("front_content")
            </div>
        </div>
    </div>
    </div>

    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>

</body>

</html>
