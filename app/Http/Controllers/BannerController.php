<?php

namespace App\Http\Controllers;

use App\Http\Requests\ValidateCreateBanner;
use App\Http\Requests\ValidateUpdateBanner;
use App\Models\Banner;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class BannerController extends Controller
{

    /**
     * Get ListBanner :
     * @param null
     * @return Page ,$banners
     * **/
    function listBanner() {
        $banners = Banner::all();
        return view('admin.pages.list_project', compact('banners'));
    }

    /**
     * Create Banner :
     * @param $request
     * @return Page
     * **/
    function createBanner(ValidateCreateBanner $request) {

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file_name = strval(strtotime(now())) . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path('/picture/banner');
            $file->move($destinationPath, $file_name);
        }

        DB::table('banners')->insert([
            'name' => $request->name,
            'slug' => Str::slug($request->name),
            'picture' => $file_name
        ]);
        return redirect()->back();
    }

    /**
     * Get ReadBanner :
     * @param $id
     * @return Page ,$project
     * **/
    function readBanner($id) {
        $project = Banner::findOrFail($id);
        return view('admin.pages.detail_project', compact('project'));
    }

    /**
     * Update Banner :
     * @param $id ,$request
     * @return Page
     * **/
    function updateBanner(ValidateUpdateBanner $request, $id) {
        $project = Banner::findOrFail($id);
        $project->name = $request->name;
        if ($request->hasFile('image')) {

            $file = $request->file('image');
            $this->deleteImage($project->picture);
            $file_name = strval(strtotime(now())) . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path('/picture/banner');
            $file->move($destinationPath, $file_name);
            $project->picture = $file_name;
        }
        $project->save();
        return redirect('/banners');
    }

    /**
     * Delete Banner :
     * @param $id
     * @return Page
     * **/
    function deleteBanner($id) {
        $project = Banner::findOrFail($id);
        $this->deleteImage($project->picture);
        $project->delete();
        return redirect()->back();
    }

    /**
     * Delete Image :
     * @param $id
     * @return true
     * **/
    public function deleteImage($imageName){
        $path = public_path() . "/picture/banner/";

        $oldImage = $path.$imageName;

        if(isset($imageName) && file_exists($oldImage)){
            //Delete old imageName
            unlink($oldImage);
        }

        return true;
    }
}
