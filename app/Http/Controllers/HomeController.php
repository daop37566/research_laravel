<?php

namespace App\Http\Controllers;

use App\Events\CustomerOrder;
use App\Events\MessageSendEvent;
use App\Jobs\WidgetJob;
use App\Models\Blog;
use App\Models\Comment;
use App\Models\Deployment;
use App\Models\Environment;
use App\Models\Films;
use App\Models\Message;
use App\Models\Post;
use App\Models\Project;
use App\Models\Tag;
use App\Models\User;
use App\Models\Video;
use App\Notifications\InvoicePaid;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Notification;

class HomeController extends Controller
{

    protected $pathView = "enduser.pages.";
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $user = Auth::user()->password ;
        // $userDe = Crypt::decrypt($user);
        // $keyword = NULL;
        // $slidebars = DB::table('banners')
        // ->when(!empty($keyword), function($q) use ($keyword) {
        //     $q->where('name', 'like', '%' . $keyword . '%');
        // })
        // ->where([
        //     ['status', 'active']
        // ])->orderBy('id')->limit(2)->get();

        // // dd($slidebars);


        // $oddmovie  = DB::table('films')->join('kind_of_film', 'films.kindoffilm_id', '=', 'kind_of_film.id')->where('kind_of_film.slug','phim-le')->get();
        // $seriesmovie  = DB::table('films')->join('kind_of_film', 'films.kindoffilm_id', '=', 'kind_of_film.id')->where('kind_of_film.slug','phim-bo')->get();


        // $pagination = $this->paginate($seriesmovie,$perPage = 50);

        // \DB::table('films')
        //     ->where('id', '<', 2)
        //     ->update(['year' => 2021]);

        // dd($pagination->lastPage());



        // $films = Films::count();
        // $pagination = new LengthAwarePaginator($films1 , $films , 6);
        // dd($pagination->items());

        // $filmDetail = Films::where('id',4)->first();
        // $listFilm =  $filmDetail -> watchfilms ;
        // dd($listFilm);


        // $films = DB::table('films')
        // ->select(DB::raw('count(*) as films_count'))
        // ->where('kindoffilm_id', 2 )
        // ->groupBy('id')
        // ->get();
        // dd($films);

        // QUAN HE T4 HAS ONE THROUGH




        // QUAN HE T5

        // $tag = Tag::find(1);
        // dd($tag->posts);


        // $post = Post::create([
        //     'title' => 'post title 1'
        // ]);

        // $post->tags()->create([
        //     'name' => 'Laravel'
        // ]);


        // $post = Post::find(1);

        // $tag = Tag::create([
        //     'name' => 'PHP'

        // ]);

        // $post->tags ()->attach($tag);

        // $video = Video::create([
        //     'name' => 'video title 1'

        // ]);
        // $tag = Tag::find(1);
        // $tag->videos()->create([
        //     'name' =>'video title 3'
        // ]);

        // dd($tag->videos);
        // $video->tags()->attach($tag);

        // dd($post->tags);

        // $slidebars = Banner::where([
        //     ['status', 'active']
        // ])->orderBy('id')->limit(2)->get();
        // $phimle  = Kindoffilms::where('slug','phim-le')->first();
        // $phimbo  = Kindoffilms::where('slug','phim-bo')->first();
        // $oddmovie  = $phimle->films()->get();
        // $seriesmovie  = $phimbo->films()->get();

        $user = Auth::user();
        $messages = Message::all();
        return view($this -> pathView . "index" ,compact("messages"));
    }

    /**
     * create Relationship  : hasManyThrough
     * @param $items , $perPage , $page , $option
     * @return $data
     * **/
    public function hasMany(Request $request)
    {
        // Create :Data Test

        // Project::create([
        //     'name' => 'Project 1'
        // ]);
        // Project::create([
        //     'name' => 'Project 2'
        // ]);

        // Environment::create([
        //     'name' => 'Env 1',
        //     'project_id' => 1
        // ]);
        // Environment::create([
        //     'name' => 'Env 2',
        //     'project_id' => 1
        // ]);
        // Environment::create([
        //     'name' => 'Env 3',
        //     'project_id' => 2
        // ]);

        // Deployment::create([
        //     'environment_id' => 1,
        //     'commit_hash'   => 'one cm'
        // ]);

        // Deployment::create([
        //     'environment_id' => 1,
        //     'commit_hash'   => 'two cm'
        // ]);

        // Deployment::create([
        //     'environment_id' => 2,
        //     'commit_hash'   => 'three cm'
        // ]);

        // Deployment::create([
        //     'environment_id' => 3,
        //     'commit_hash'   => 'four cm'
        // ]);

        $project = Films::find(2);

        echo $project->deployments ;
        $fims = Films::get();
        foreach($fims as $i){
            $i->cong_ty = 'Coisubin';
        }
        $data = $this->paginate($fims);
        dd($data);


    }

    /**
     * create demo Call Event :
     * @param null
     * @return null
     * **/
    public function demoEvent()
    {
        $project = Films::find(2);
        event(new CustomerOrder($project));
    }

    /**
     * demo Send Mail Notification
     * @param No
     * @return sendMail
     * **/

    public function demoNotification()
    {

        // Return : 1 record User
        $user = User::find(2);

        $enrollmentData = [
            'body' => 'You received an new test notification',
            'enrollmentText' => 'You are allowed to enroll',
            'url' => url('/has-many'),
            'thankyou' => 'You have 14 days to enroll'
        ];

        // $user->notify(new InvoicePaid($enrollmentData));

        Notification::send ($user, new InvoicePaid($enrollmentData));
    }

    /**
     * demo Relationshop : Many to many
     * @param No
     * @return Print name Comment
     * **/
    public function morphoTo()
    {
        // Fake DB : Test
        // $blog = Blog::create([
        //     'name' => 'Blog 1'
        // ]);

        // $blog->comments()->create([
        //     'name' => 'Comment blog 1',
        // ]);

        // $comment = Comment::find(1);

        // $comment->comments()->create([
        //     'name' => 'Comment 1 of cmt 2',
        // ]);

        $blog = Blog::find(1);

        foreach($blog->comments as $comment){
            foreach($comment->comments as $c){
                echo "</br>". $c->name;
             }
        }

        // QUEUE .................
        // WidgetJob::dispatch()->delay(now()->addMinutes(1));

        // echo 'ra đi nào người anh em';
    }


    /**
     * create Pagination :
     * @param $items , $perPage , $page , $option
     * @return $data
     * **/
    public function paginate($items,$perPage = 3, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    /**
     * create save message :
     * @param $request
     * @return $data  Type:Json
     * **/
    public function store(Request $request)
    {
        $user = Auth::user();
        $message = Message::create([
            'user_id' => Auth::id(),
            'message' => $request->message,
        ]);
        event(new MessageSendEvent($user, $message));

        return response()->json([
            'status' => 1,
        ]);
    }


    /**
     * create Send SMS Notification :
     * @param null
     * @return echo $nofity
     * **/
    public function sendSmsNotificaition()
    {
        $basic  = new \Vonage\Client\Credentials\Basic("ebef5f58", "IhTfwn2ciyiKUuzZ");
        $client = new \Vonage\Client($basic);
        $response = $client->sms()->send(
            new \Vonage\SMS\Message\SMS("84397552977", '0397552977', 'A text message sent using the Nexmo SMS API')
        );

        $message = $response->current();

        if ($message->getStatus() == 0) {
            echo "The message was sent successfully\n";
        } else {
            echo "The message failed with status: " . $message->getStatus() . "\n";
        }
    }
}



?>
