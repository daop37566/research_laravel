<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = "blogs";
    protected $fillable = ['name'];

    public function comments() {
        return $this->morphMany(Comment::class, 'commentable');
    }
}