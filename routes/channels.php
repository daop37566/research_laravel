<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/
Broadcast::channel('App.Models.User.{userId}', function ($user, $userId) {
    return $user->id === $userId;
  });


// Khai báo kênh broadcast
Broadcast::channel('message', function () {
    return true;
});

// Broadcast::channel('chat', function ($user) {
//     return Auth::check();
// });

// use App\Broadcasting\OrderChannel;

// Broadcast::channel('orders.{order}', OrderChannel::class);

