<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// $guzzle = new GuzzleHttp\Client;

// $response = $guzzle->post('http://127.0.0.1:8000/oauth/token', [
//     'form_params' => [
//         'grant_type' => 'client_credentials',
//         'client_id' => 'client-id',
//         'client_secret' => 'client-secret',
//         'scope' => 'your-scope',
//     ],
// ]);

// return json_decode((string) $response->getBody(), true)['access_token'];

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace("Api")->group(function (){

    Route::get('/redirect', function (Request $request) {

        $query = http_build_query([
            'client_id' => 'client-id',
            'redirect_uri' => 'http://example.com/callback',
            'response_type' => 'token',
            'scope' => '',
            'state' => Str::random(40),
        ]);

        return redirect('http://127.0.0.1:8000/oauth/authorize?'.$query);
    });


    /*------- User Api -------*/
    $prefix = "user";
    $controllerName = "user";
    Route::prefix($prefix)->name($controllerName . ".")->group(function () use ($controllerName){
        $controller = ucfirst($controllerName) . "Controller@";
        Route::get("/", $controller . "index")->name("index");
    });

    $prefix = "auth";
    Route::prefix($prefix)->group(function () {
        Route::post("/register", "User" . "ApiController@postRegister");
        Route::post("/login", "User" . "ApiController@login");

        Route::post('oauth/access_token', 'OAuthController@postAccessToken');

        Route::get("/loginSocial", "User" . "ApiController@redirectToProvider");
    });

    Route::group(['middleware' => ['client']], function(){
        Route::get('/orders', function (Request $request) {
            return '123';
        });
        Route::get('/show-list', function (Request $request) {
            return '12345';
        });

    });


    Route::group(['middleware' => ['auth:api','auth.admin_role']], function(){
        Route::get('/detail-home', function(){
            echo '123';
        });
    });
});


Route::get('/auth/passport', function () {
    $query = http_build_query([
       'client_id' => '231154190421-e5qef4a81lv562aebaiao8dk4se1qt8s.apps.googleusercontent.com',
       'redirect_uri' => 'http://127.0.0.1:8000/google/callback',
       'response_type' => 'code',
       'scope' => ''
     ]);

    return redirect('https://accounts.google.com/o/oauth2/authorize?'.$query);
});

Route::get('/callback', function (Request $request) {
   $http = new GuzzleHttp\Client;

   $response = $http->get('http://webfilm-online.surge.sh/', [
   ]);

   return json_decode((string) $response->getBody(), true);
});

Route::group(['middleware' => 'JWTAuth'], function () {
    Route::get('user-info', function()
{
    return "123";
});
});
